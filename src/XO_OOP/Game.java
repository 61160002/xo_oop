package XO_OOP;

import java.util.Scanner;

public class Game {
	private Table table;
	private int row;
	private int col;
	private Player O;
	private Player X;

	public Game() {
		this.X = new Player('X');
		this.O = new Player('O');
	}
	
	public boolean runOnce() {
		for (;;) {
			showTable(table.getTable());
			showTurn();
			inputRowCol(table);
			if (table.checkWin()) {
				if(table.getWinner() != ' ') {
					showWin();
					return true;
				}else {
					showDraw();
					return true;
				}
			}
			table.switchTurn(table.getNameCurrentPlayer());
		}
	}

	public void run() {
		table = new Table(X, O);
		showWelcome();
		table.randomStartPlayer();
		if(runOnce()) {
			showPlayAgain();
		}
	}

	private void showPlayAgain() {
		Scanner kb = new Scanner(System.in);
		System.out.print("Do you want to play again? (y/n) : ");
		char ans = kb.next().charAt(0);
		if(ans == 'y') {
			run();
		}else {
			showBye();
		}
	}

	private void showBye() {
		System.out.println("Thank you!! Bye~");
		
	}

	public void showWelcome() {
		System.out.println("!! Welcome to XO Game !!");
	}
	
	public void showTurn() {
		System.out.println("It's " + table.getNameCurrentPlayer() + " turn");
	}

	public void showTable(char[][] table) {
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				System.out.print(table[i][j] + " ");
			}
			System.out.println();
		}
	}

	public boolean inputRowCol(Table table) {
		Scanner kb = new Scanner(System.in);
		for (;;) {
			System.out.print("please input row ,colomn : ");
			row = kb.nextInt();
			col = kb.nextInt();

			if (checkBound(row, col)) {
				if (checkNull(row, col, table, table.getNameCurrentPlayer())) {
					return true;
				}
			}
		}
	}

	public boolean checkBound(int r, int c) {
		for (;;) {
			if ((r >= 1 && r <= 3) && (c >= 1 && c <= 3)) {
				return true;
			} else {
				System.out.println("You can input only 1 2 or 3");
				return false;
			}
		}
	}

	public boolean checkNull(int r, int c, Table table, char turn) {
		for (;;) {
			if (table.getTable()[r - 1][c - 1] == '-') {
				table.getTable()[r - 1][c - 1] = turn;
				return true;
			} else {
				System.out.println("Not null! input again");
				return false;
			}
		}
	}
	
	public void showWin() {
		showTable(table.getTable());
		System.out.println(table.getWinner()+" is Winner");
		if(table.getWinner() == 'X') {
			X.addWin();
			O.addLose();
		}else {
			O.addWin();
			X.addLose();
		}
	}
	
	public void showDraw() {
		showTable(table.getTable());
		System.out.println("You are Draw!");
		X.addDraw();
		O.addDraw();
	}

}
