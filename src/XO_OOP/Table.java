package XO_OOP;

import java.util.Random;

public class Table {
	private char[][] table = new char[][] { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
	private Player currentPlayer;
	private Player o;
	private Player x;
	private char win = ' ';
	private int countTurn = 1;

	Table(Player x, Player o) {
		this.x = x;
		this.o = o;
	}

	public void randomStartPlayer() {
		currentPlayer = new Player();
		String chars = "XO";
		Random r = new Random();
		char c = chars.charAt(r.nextInt(chars.length()));
		currentPlayer.setName(c);
	}

	public char getNameCurrentPlayer() {
		return currentPlayer.getName();
	}

	public char[][] getTable() {
		return table;
	}

	public boolean checkWin() {
		if (checkRow(table)) {
			this.win = currentPlayer.getName();
			return true;
		} else if (checkCol(table)) {
			this.win = currentPlayer.getName();
			return true;
		} else if (checkDiag(table)) {
			this.win = currentPlayer.getName();
			return true;
		} else if(countTurn == 9){
			return true;
		}else {
			return false;
		}
	}

	private boolean checkDiag(char[][] table) {
		if (table[0][0] == table[1][1] && table[1][1] == table[2][2] && table[0][0] != '-') {
			return true;
		} else if (table[2][0] == table[1][1] && table[1][1] == table[0][2] && table[2][0] != '-') {
			return true;
		} else {
			return false;
		}
	}

	private boolean checkCol(char[][] table) {
		if (table[0][0] == table[1][0] && table[1][0] == table[2][0] && table[0][0] != '-') {
			return true;
		} else if (table[0][1] == table[1][1] && table[1][1] == table[2][1] && table[0][1] != '-') {
			return true;
		} else if (table[0][2] == table[1][2] && table[1][2] == table[2][2] && table[0][2] != '-') {
			return true;
		} else {
			return false;
		}
	}

	private boolean checkRow(char[][] table) {
		if (table[0][0] == table[0][1] && table[0][1] == table[0][2] && table[0][0] != '-') {
			return true;
		} else if (table[1][0] == table[1][1] && table[1][1] == table[1][2] && table[1][0] != '-') {
			return true;
		} else if (table[2][0] == table[2][1] && table[2][1] == table[2][2] && table[2][0] != '-') {
			return true;
		} else {
			return false;
		}
	}

	public void switchTurn(char currentPlayer) {
		if (this.currentPlayer.getName() == 'X') {
			this.currentPlayer.setName('O');
		} else {
			this.currentPlayer.setName('X');
		}
		countTurn++;
	}
	
	public char getWinner() {
		return win;
	}

}
