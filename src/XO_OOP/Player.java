package XO_OOP;

public class Player {
	private char name;
	private int win;
	private int lose;
	private int draw;

	public Player() {
		
	}
	
	public Player(char name) {
		this.name = name;
	}
	
	public char getName() {
		return name;
	}

	public void setName(char name) {
		this.name = name;
	}
	
	public void addWin() {
		win++;
	}
	
	public void addLose() {
		lose++;
	}
	
	public void addDraw() {
		draw++;
	}

}
